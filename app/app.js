var express = require('express');
var bodyparser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var app = express();

var todos = [];
var users = [
  {id: 1, username: 'joseph', password: 'password'},
  {id: 2, username: 'guest', password: 'password'}
];
var count = 1;

app.set('view engine', 'jade');
app.set('views', 'app');

app.use(express.static('public'));
app.use(bodyparser.urlencoded());
app.use(session({secret: 'secret', name: 'macaroon', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());

passport.use('local', new LocalStrategy(
  function(username, password, done) {
    var user, i;

    for (i = 0; i < users.length; i++) {
      if (users[i].username == username) {
	break;
      }
    }

    user = users[i];
    if (user.username !== username) {
      return done(null, false, {message: 'Username doesn\'t exist'});
    }

    if (user.password !== password) {
      return done(null, false, {message: 'Invalid password'});
    }
    return done(null, user);
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  var user, i;

  for (i = 0; i < users.length; i++) {
    if (users[i].id === id) {
      break;
    }
  }

  user = users[i];
  done(null, user);
});

app.route('/todos')
  .all(function(req, res, next) {
    if (!req.user) return res.status(401).end();
    next();
  })
  .post(function(req, res) {
    var todo;

    todo = JSON.parse(req.body.model);
    todo.id = count++;
    todos.push(todo);

    res.json(todo);
  })
  .get(function(req, res) {
    res.json(todos);
  });


app.route('/todos/:id')
  .all(function(req, res, next) {
    if (!req.user) return res.status(401).end();
    next();
  })
  .get(function(req, res) {
    var id,
	i;

    id = +req.params.id;
    for (i = 0; i < todos.length; i++) {
      if (todos[i].id === id) {
	break;
      }
    }

    res.json(todos[i]);
  })
  .put(function(req, res) {
    var todo,
	id,
	i;

    id = +req.params.id;
    todo = JSON.parse(req.body.model);
    for (i = 0; i < todos.length; i++) {
      if (todos[i].id === id) {
	break;
      }
    }

    todos[i].title = todo.title;
    todos[i].done = todo.done;

    res.json(todos[i]);
  })
  .delete(function(req, res) {
    var todo,
	id,
	i;

    id = +req.params.id;
    for (i = 0; i < todos.length; i++) {
      if (todos[i].id === id) {
	break;
      }
    }

    todo = todos[i];
    todos.splice(i, 1);
    res.json(todo);
  });

app.get('/', function(req, res) {
  if (!req.user) return res.redirect('/login');

  res.render('index', {
    title: 'Express.js Todos'
  });
});

app.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
}));

app.get('/login', function(req, res) {
  res.render('login', {
    title: 'Express.js Todos - login'
  })
});

module.exports = app;
