var app = require('./app');
var port = 8080;

app.listen(port, function() {
  console.log('running on port', port);
});
